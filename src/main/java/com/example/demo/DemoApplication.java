package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DemoApplication {
	public static void main(String[] args) {
		
		ApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);

		System.out.println("---");
		SavingsApp savingsApp = new SavingsApp();
		savingsApp.displayBalance("J95455", 9474);

		System.out.println("---");
		LoanApp loanApp = new LoanApp();
		loanApp.displayLoan("a0375", "mortgage", 239481);
	}

}