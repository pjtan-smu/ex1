package com.example.demo;

public class SavingsApp {
    
    private StdFormatService formatter;

    public SavingsApp() {
        this.formatter = new StdFormatService();
    }

    public void displayBalance(String account, int balance) {
        System.out.println("account: " + account);
        System.out.println("balance: " + formatter.format(balance));
    }

}
