package com.example.demo;

public class LoanApp {
    
    private CurrencyFormatService formatter;

    public LoanApp() {
        this.formatter = new CurrencyFormatService(); 
    }

    public void displayLoan(String account, String type, int loan) {
        System.out.println("account: " + account);
        System.out.println("type: " + type);
        System.out.println("outstanding: " + formatter.formatNumber(loan));
    }

}